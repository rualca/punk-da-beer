import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  form: FormGroup;

  constructor(
    private fb:FormBuilder, 
    private authService: AuthService, 
    private router: Router
  ) {}

  ngOnInit(): void {
    this.form = this.fb.group({
      username: ['',Validators.required],
      password: ['',Validators.required]
    });
  }

  /**
   * If the form is invalid, alert the user. Otherwise, get the username and password from the form,
   * and call the login function on the authService. If the login is successful, navigate to the beers
   * page
   */
  submit() {
    if (this.form.invalid) {
      alert('Incorrect form values');
    }
    const {value: {username, password}} = this.form;

    this.authService.login(username, password)
      .subscribe(
        () => {
          console.log("User is logged in");
          this.router.navigateByUrl('/beers');
        }
      );
  }

}
