import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import { LoginComponent } from './login.component';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;

  beforeEach(() => {
    const formBuilderStub = () => {
      (group: object) => ({}) 
    };
    const routerStub = () => {
      (navigateByUrl: string) => {}
    };
    const authServiceStub = () => {
      login: (username: string, password: string) => {
        subscribe: (f: any) => f({}) 
      }
    };

    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      declarations: [LoginComponent],
      providers: [
        { provide: FormBuilder, useFactory: formBuilderStub },
        { provide: Router, useFactory: routerStub },
        { provide: AuthService, useFactory: authServiceStub }
      ]
    });

    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
  });

  it('can load instance', () => {
    expect(component).toBeTruthy();
  });

  // describe('ngOnInit', () => {
  //   it('makes expected calls', () => {
  //     const formBuilderStub: FormBuilder = fixture.debugElement.injector.get(
  //       FormBuilder
  //     );

  //     spyOn(formBuilderStub, 'group').and.callThrough();

  //     component.ngOnInit();

  //     expect(formBuilderStub.group).toHaveBeenCalled();
  //   });
  // });

  // describe('submit', () => {
  //   it('makes expected calls', () => {
  //     const routerStub: Router = fixture.debugElement.injector.get(Router);
  //     const authServiceStub: AuthService = fixture.debugElement.injector.get(
  //       AuthService
  //     );

  //     // spyOn(routerStub, 'navigateByUrl').and.callThrough();
  //     spyOn(authServiceStub, 'login').and.callThrough();

  //     component.submit();
      
  //     // expect(routerStub.navigateByUrl).toHaveBeenCalled();
  //     expect(authServiceStub.login).toHaveBeenCalled();
  //   });
  // });
});
