import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Beer } from 'src/app/domain/models/beer.model';
import { BeerService } from 'src/app/services/beer.service';

@Component({
  selector: 'app-beer',
  templateUrl: './beer.component.html',
  styleUrls: ['./beer.component.scss']
})
export class BeerComponent implements OnInit {
  beer: Beer;

  constructor(
    private route: ActivatedRoute,
    private beerService: BeerService
  ) { }

  ngOnInit() {
    const { beerID } = this.route.snapshot.params;

    this.getBeerById(beerID);
  }

  /**
   * We're calling the getBeerById() function from the BeerService, passing in the beerID from the URL,
   * and then assigning the beer object returned from the service to the beer property in our component
   * @param {string} beerID - string - this is the ID of the beer we want to get from the API.
   */
  private getBeerById(beerID: string) {
    this.beerService.getBeerById(beerID).subscribe(
      (beer: Beer) => this.beer = beer
    )
  }

}
