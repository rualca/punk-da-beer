import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { BeerRoutingModule } from './beer-routing.module';
import { BeerComponent } from '../beer/beer.component';
import { BeersComponent } from './beers.component';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatRippleModule } from '@angular/material/core';
import { MatDividerModule } from '@angular/material/divider';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatExpansionModule} from '@angular/material/expansion';



@NgModule({
  declarations: [
    BeersComponent,
    BeerComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    BeerRoutingModule,
    MatListModule,
    MatDividerModule,
    MatIconModule,
    MatRippleModule,
    MatGridListModule,
    MatExpansionModule,
  ],
  providers: [],
})
export class BeerModule {}
