import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { map, Observable, Subject } from 'rxjs';
import { Beer } from 'src/app/domain/models/beer.model';
import { BeerService } from 'src/app/services/beer.service';

@Component({
  selector: 'app-beers',
  templateUrl: './beers.component.html',
  styleUrls: ['./beers.component.scss']
})
export class BeersComponent implements OnInit {
  originalBeers$: Observable<Beer[]>;
  selectionBeers$: Observable<Beer[]>;

  get top10SelectionBeers() {
    return this.selectionBeers$
    .pipe(
      map((beers) => beers.slice(-10)),
      map(
        (beers) => beers.sort(
          (beerA, beerB) => {
            const beerAFirstBrewedDateParsed = new Date(beerA.first_brewed);
            const beerBFirstBrewedDateParsed = new Date(beerB.first_brewed);

            return beerBFirstBrewedDateParsed.getTime() - beerAFirstBrewedDateParsed.getTime();
          }
        )
      ),
    );
  }

  constructor(
    private beerService: BeerService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.originalBeers$ = this.beerService.getOriginalBeers();
    this.selectionBeers$ = this.beerService.getSelectionBeers();

    this.beerService.getAllBeers();
  }

  /**
   * The function takes an id as a parameter, and then uses the router to navigate to the beers route,
   * and passes the id as a parameter
   * @param {string} id - The id of the beer to go to.
   */
  goToBeer(id: string) {
    this.router.navigate(['beers', id]);
  }

}
