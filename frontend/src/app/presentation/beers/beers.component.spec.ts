import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { Router } from '@angular/router';
import { BeerService } from 'src/app/services/beer.service';
import { BeersComponent } from './beers.component';

describe('BeersComponent', () => {
  let component: BeersComponent;
  let fixture: ComponentFixture<BeersComponent>;

  beforeEach(() => {
    const routerStub = () => ({ navigate: (array: any) => ({}) });
    const beerServiceStub = () => ({
      getOriginalBeers: () => ({}),
      getSelectionBeers: () => ({}),
      getAllBeers: () => ({})
    });

    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      declarations: [BeersComponent],
      providers: [
        { provide: Router, useFactory: routerStub },
        { provide: BeerService, useFactory: beerServiceStub }
      ]
    });
    
    fixture = TestBed.createComponent(BeersComponent);
    component = fixture.componentInstance;
  });

  it('can load instance', () => {
    expect(component).toBeTruthy();
  });

  describe('ngOnInit', () => {
    it('makes expected calls', () => {
      const beerServiceStub: BeerService = fixture.debugElement.injector.get(
        BeerService
      );

      spyOn(beerServiceStub, 'getOriginalBeers').and.callThrough();
      spyOn(beerServiceStub, 'getSelectionBeers').and.callThrough();
      spyOn(beerServiceStub, 'getAllBeers').and.callThrough();

      component.ngOnInit();
      
      expect(beerServiceStub.getOriginalBeers).toHaveBeenCalled();
      expect(beerServiceStub.getSelectionBeers).toHaveBeenCalled();
      expect(beerServiceStub.getAllBeers).toHaveBeenCalled();
    });
  });
});
