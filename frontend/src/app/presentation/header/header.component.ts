import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { NavigationEnd, Router } from '@angular/router';
import { filter, map, Observable, startWith } from 'rxjs';
import { AuthService } from 'src/app/services/auth.service';
import { BeerService } from 'src/app/services/beer.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  control = new FormControl();
  filteredBeers: Observable<string[]>;
  userIsAuthenticated$: Observable<boolean>;
  currentUrl = '';

  constructor(
    private authService: AuthService,
    private router: Router,
    private beerService: BeerService
  ) { }

  ngOnInit(): void {
    this.userIsAuthenticated$ = this.authService.isUserAuthenticated();
    this.control.valueChanges.pipe(
      startWith(''),
    )
    .subscribe(
      (text) => this._filter(text)
    );

    this.router.events
      .pipe(
        filter((event) => event instanceof NavigationEnd)
      )
      .subscribe(
        (event: any) => 
          this.currentUrl = event.url
      );
  }

  /**
   * The function takes a string as an argument, normalizes it, and then passes it to the beer service
   * to filter the beer list
   * @param {string} value - string - The value of the input field
   */
  private _filter(value: string) {
    const filterValue = this._normalizeValue(value);
    this.beerService.filterBeerByText(filterValue);
  }

  /**
   * It takes a string, converts it to lowercase, and removes all spaces
   * @param {string} value - The value to be normalized.
   * @returns The value of the input field, lowercased and with all spaces removed.
   */
  private _normalizeValue(value: string): string {
    return value.toLowerCase().replace(/\s/g, '');
  }

  /**
   * The logout function calls the logout function in the auth service
   */
  logout() {
    this.authService.logout();
  }

}
