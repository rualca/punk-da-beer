import { TestBed } from "@angular/core/testing";
import {
  HttpClientTestingModule,
  HttpTestingController
} from "@angular/common/http/testing";
import { LocalStorageService } from "./localstorage.service";
import { BeerService } from "./beer.service";

describe("BeerService", () => {
  let service: BeerService;

  beforeEach(() => {
    const localStorageServiceStub = () => ({});

    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        BeerService,
        { provide: LocalStorageService, useFactory: localStorageServiceStub }
      ]
    });

    service = TestBed.inject(BeerService);
  });

  it("can load instance", () => {
    expect(service).toBeTruthy();
  });

  describe("getAllBeers", () => {
    it("makes expected calls", () => {
      const httpTestingController = TestBed.inject(HttpTestingController);

      service.getAllBeers();

      const req = httpTestingController.expectOne(
        "http://localhost:3001/api/beer"
      );

      expect(req.request.method).toEqual("GET");

      // req.flush();
      
      httpTestingController.verify();
    });
  });

  describe("getBeersAutomcomplete", () => {
    it("makes expected calls", () => {
      const httpTestingController = TestBed.inject(HttpTestingController);

      service.getBeersAutomcomplete('Buzz');

      const req = httpTestingController.expectOne(
        "http://localhost:3001/api/beer/autocomplete/Buzz"
      );

      expect(req.request.method).toEqual("GET");

      // req.flush();
      
      httpTestingController.verify();
    });
  });

  describe("getBeerById", () => {
    it("makes expected calls", () => {
      const httpTestingController = TestBed.inject(HttpTestingController);

      service.getBeerById('625718080df1e6edf6b4ff79').subscribe(
        (res) => {}
      );

      const req = httpTestingController.expectOne(
        "http://localhost:3001/api/beer/625718080df1e6edf6b4ff79"
      );

      expect(req.request.method).toEqual("GET");

      // req.flush();
      
      httpTestingController.verify();
    });
  });
});
