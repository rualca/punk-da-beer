import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { BehaviorSubject, Observable, tap } from "rxjs";
import { LocalStorageService } from "./localstorage.service";

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  userIsAuthenticated$ = new BehaviorSubject<boolean>(false);

  constructor(
    private http: HttpClient,
    private localStorageService: LocalStorageService,
    private router: Router
  ) { }
    
  /**
   * We're sending a POST request to the server with the email and password, and if the request is
   * successful, we're saving the token in the local storage and emitting a new value for the
   * userIsAuthenticated$ subject
   * @param {string} email - The email address of the user.
   * @param {string} password - The password of the user.
   * @returns The token is being returned.
   */
  login(email:string, password:string ) {
    return this.http
      .post('http://localhost:3001/api/auth/login', {email, password})
      .pipe(
        tap(
          (data: any) => {
            this.localStorageService.set('token', data.access_token);
            this.userIsAuthenticated$.next(true);
          }
        )
      );
  }

  /**
   * It returns the token from local storage, or null if there is no token
   * @returns The token from local storage.
   */
  getAuthToken(): string | null {
    return localStorage.getItem('token');
  }

  /**
   * It returns an observable that emits a boolean value
   * @returns Observable<boolean>
   */
  isUserAuthenticated(): Observable<boolean> {
    return this.userIsAuthenticated$.asObservable();
  }

  /**
   * If the user is authenticated, then the userIsAuthenticated$ observable will emit a true value
   * @returns A boolean value.
   */
  isAuthenticated() {
    const token = localStorage.getItem('token');

    if (!token) {
      this.userIsAuthenticated$.next(false);

      return;
    }

    this.userIsAuthenticated$.next(true);
    
    return token;
  }

  /**
   * We're removing the token from local storage and setting the userIsAuthenticated$ observable to
   * false
   */
  logout() {
    localStorage.removeItem('token');
    this.userIsAuthenticated$.next(false);

    this.router.navigate(['login']);
  }
}