import { TestBed } from '@angular/core/testing';
import { LocalStorageService } from './localstorage.service';

describe('LocalStorageService', () => {
  let service: LocalStorageService;

  beforeEach(() => {
    TestBed.configureTestingModule({ providers: [LocalStorageService] });
    service = TestBed.inject(LocalStorageService);
  });

  it('can load instance', () => {
    expect(service).toBeTruthy();
  });
});
