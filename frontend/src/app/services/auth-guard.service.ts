import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root',
})
export class AuthGuardService implements CanActivate {
  constructor(
    public authService: AuthService,
    public router: Router
  ) {}

  /**
   * If the user is not authenticated, redirect them to the login page and return false. Otherwise,
   * return true
   * @returns A boolean value.
   */
  canActivate(): boolean {
    if (!this.authService.isAuthenticated()) {
      this.router.navigate(['login']);

      return false;
    }

    return true;
  }
}