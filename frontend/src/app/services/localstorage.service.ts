import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class LocalStorageService {
  constructor() {}

  /**
   * It gets a value from localStorage, parses it, and returns the result
   * @param {string} key - The key of the item you want to get from localStorage.
   * @returns The data is being returned as a string or null.
   */
  get(key: string) {
    const data: string | null = localStorage.getItem(key);
    return this.parse(data);
  }

  /**
   * It takes a key and a value, and sets the value in localStorage
   * @param {string} key - The key to store the value under.
   * @param {any} value - any
   */
  set(key: string, value: any): void {
    localStorage.setItem(
      key,
      typeof value === 'object' ? JSON.stringify(value) : value,
    );
  }

  /**
   * It removes a key from localStorage if it exists, otherwise it logs a message to the console
   * @param {string} key - The key of the item you want to remove.
   */
  remove(key: string): void {
    if (localStorage[key]) {
      localStorage.removeItem(key);
    } else {
      console.log('Trying to remove unexisting key: ', key);
    }
  }

  /**
   * It clears the local storage
   */
  removeAll() {
    localStorage.clear();
  }

  /**
   * It tries to parse the value as JSON, and if it fails, it returns the value as is
   * @param {any} value - The value to be parsed.
   * @returns The value of the key in the localStorage.
   */
  private parse(value: any) {
    try {
      return JSON.parse(value);
    } catch (e) {
      return value;
    }
  }
}
