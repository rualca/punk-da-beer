import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { BehaviorSubject, Observable, tap } from "rxjs";
import { Beer } from "../domain/models/beer.model";
import { LocalStorageService } from "./localstorage.service";

@Injectable({
  providedIn: 'root',
})
export class BeerService {
  originalBeers$: BehaviorSubject<Beer[]> = new BehaviorSubject<Beer[]>([]);
  selectionBeers$: BehaviorSubject<Beer[]> = new BehaviorSubject<Beer[]>([]);

  private baseUri = 'http://localhost:3001/api/beer';

  constructor(
    private http: HttpClient,
    private localStorageService: LocalStorageService
  ) { }

  /**
   * It returns an observable of the original beers
   * @returns An observable of the original beers.
   */
  getOriginalBeers() {
    return this.originalBeers$.asObservable();
  }

  /**
   * It returns an observable of the selectionBeers$ property
   * @returns An observable of the selectionBeers$
   */
  getSelectionBeers() {
    return this.selectionBeers$.asObservable();
  }

  /**
   * We're using the HTTP client to get an array of beers from the API, and then we're subscribing to
   * the observable that the HTTP client returns. 
   * 
   * When the observable emits a value, we're going to take that value and put it into the
   * originalBeers$ subject. 
   * 
   * We're also going to put that value into the selectionBeers$ subject. 
   * 
   * The reason we're doing this is because we want to have a copy of the original beers that we can
   * use to filter the beers. 
   * 
   * If we didn't have a copy of the original beers, we'd have to make another HTTP request to the API
   * to get the original beers. 
   * 
   * We don't want to do that because it would be a waste of resources. 
   * 
   * So, we're going to make a copy of the original beers and put that copy into the selectionBeers$
   * subject. 
   * 
   * We're going to use the selectionBe
   */
  getAllBeers() {
    this.http.get<Beer[]>(this.baseUri)
      .subscribe(
        (beers: Beer[]) => {
          this.originalBeers$.next(beers);
          this.selectionBeers$.next(beers);
        }
      );
  }

  /**
   * The function takes a string as an argument and calls the getBeersAutomcomplete function with the
   * string as an argument
   * @param {string} text - The text that the user has typed in the search bar.
   */
  filterBeerByText(text: string) {
    this.getBeersAutomcomplete(text);
  }

  /**
   * We're using the `http` service to make a `GET` request to the `autocomplete` endpoint of the `Punk
   * API` and we're passing the `text` parameter to the `autocomplete` endpoint
   * @param {string} text - string - the text that the user has typed into the search box
   */
  getBeersAutomcomplete(text: string) {
    if (!text) {
      this.getAllBeers();

      return;
    }

    const url = `${this.baseUri}/autocomplete/`;

    this.http.get<Beer[]>(`${url}${text}`)
      .subscribe(
        (beers: Beer[]) => {
          this.selectionBeers$.next(beers);
        }
      );
  }

  /**
   * "Get a beer by its id."
   * 
   * The first line of the function is a TypeScript type annotation. It says that the function takes a
   * single parameter, id, which is a string. The function returns a Beer object
   * @param {string} id - The id of the beer to retrieve.
   * @returns Observable<Beer>
   */
  getBeerById(id: string) {
    const url = `${this.baseUri}/${id}`;

    return this.http.get<Beer>(url);
  }
}
