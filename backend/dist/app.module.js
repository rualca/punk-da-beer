"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppModule = void 0;
const common_1 = require("@nestjs/common");
const core_1 = require("@nestjs/core");
const controllers_1 = require("./controllers");
const user_controller_1 = require("./controllers/user.controller");
const auth_module_1 = require("./frameworks/auth-service/passport/auth.module");
const jwt_auth_guard_1 = require("./frameworks/auth-service/passport/jwt-auth.guard");
const logger_middleware_1 = require("./middleware/logger.middleware");
const data_service_1 = require("./services/data-service/data.service");
const beer_module_1 = require("./services/use-cases/beer/beer.module");
const client_request_module_1 = require("./services/use-cases/client-request/client-request.module");
let AppModule = class AppModule {
    configure(consumer) {
        consumer
            .apply(logger_middleware_1.LoggerMiddleware)
            .forRoutes({ path: '*', method: common_1.RequestMethod.ALL });
    }
};
AppModule = __decorate([
    (0, common_1.Module)({
        imports: [
            data_service_1.DataServicesModule,
            beer_module_1.BeerServicesModule,
            auth_module_1.AuthModule,
            client_request_module_1.ClientServicesModule,
        ],
        controllers: [controllers_1.BeerController, user_controller_1.UserController],
        providers: [
            {
                provide: core_1.APP_GUARD,
                useExisting: jwt_auth_guard_1.JwtAuthGuard,
            },
            jwt_auth_guard_1.JwtAuthGuard
        ],
    })
], AppModule);
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map