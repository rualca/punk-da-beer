/// <reference types="mongoose/types/pipelinestage" />
/// <reference types="mongoose/types/connection" />
/// <reference types="mongoose/types/cursor" />
/// <reference types="mongoose/types/document" />
/// <reference types="mongoose/types/error" />
/// <reference types="mongoose/types/mongooseoptions" />
/// <reference types="mongoose/types/schemaoptions" />
import { Document } from 'mongoose';
export declare type ClientRequestDocument = ClientRequest & Document;
export declare class ClientRequest {
    method: string;
    url: string;
    headers: string[];
    body: any;
    query: any;
    params: any;
}
export declare const ClientRequestSchema: import("mongoose").Schema<Document<ClientRequest, any, any>, import("mongoose").Model<Document<ClientRequest, any, any>, any, any, any>, {}, {}>;
