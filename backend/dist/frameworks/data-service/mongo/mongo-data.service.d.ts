import { OnApplicationBootstrap } from '@nestjs/common';
import { Model } from 'mongoose';
import { IDataServices } from '../../../core';
import { Beer, BeerDocument, User, UserDocument } from './model';
import { ClientRequest, ClientRequestDocument } from './model/client-request.model';
import { MongoGenericRepository } from './mongo-generic.repository';
export declare class MongoDataServices implements IDataServices, OnApplicationBootstrap {
    private beerRepository;
    private userRepository;
    private clientRequestRepository;
    beers: MongoGenericRepository<Beer>;
    users: MongoGenericRepository<User>;
    clientRequests: MongoGenericRepository<ClientRequest>;
    constructor(beerRepository: Model<BeerDocument>, userRepository: Model<UserDocument>, clientRequestRepository: Model<ClientRequestDocument>);
    onApplicationBootstrap(): void;
}
