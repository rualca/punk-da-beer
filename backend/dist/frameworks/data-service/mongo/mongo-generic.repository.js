"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MongoGenericRepository = void 0;
class MongoGenericRepository {
    constructor(repository, populateOnFind = []) {
        this._repository = repository;
        this._populateOnFind = populateOnFind;
    }
    getAll() {
        return this._repository.find().populate(this._populateOnFind).exec();
    }
    count() {
        return this._repository.count().populate(this._populateOnFind).exec();
    }
    getById(id) {
        return this._repository.findById(id).populate(this._populateOnFind).exec();
    }
    get(filter) {
        let parsedFilter = filter;
        if (typeof filter === 'string') {
            parsedFilter = JSON.parse(filter);
        }
        return this._repository.find(parsedFilter).populate(this._populateOnFind).exec();
    }
    getOne(filter) {
        return this._repository
            .findOne(filter)
            .populate(this._populateOnFind)
            .exec();
    }
    create(item) {
        return this._repository.create(item);
    }
    createMany(items) {
        return this._repository.insertMany(items);
    }
    update(id, item) {
        return this._repository.findByIdAndUpdate(id, item);
    }
    aggregate(aggregateFilter) {
        return this._repository.aggregate(aggregateFilter);
    }
}
exports.MongoGenericRepository = MongoGenericRepository;
//# sourceMappingURL=mongo-generic.repository.js.map