/// <reference types="mongoose/types/pipelinestage" />
/// <reference types="mongoose/types/connection" />
/// <reference types="mongoose/types/cursor" />
/// <reference types="mongoose/types/document" />
/// <reference types="mongoose/types/error" />
/// <reference types="mongoose/types/mongooseoptions" />
/// <reference types="mongoose/types/schemaoptions" />
import { Model } from 'mongoose';
import { IGenericRepository } from '../../../core';
export declare class MongoGenericRepository<T> implements IGenericRepository<T> {
    private _repository;
    private _populateOnFind;
    constructor(repository: Model<T>, populateOnFind?: string[]);
    getAll(): Promise<T[]>;
    count(): Promise<number>;
    getById(id: any): Promise<any>;
    get(filter: any): Promise<any>;
    getOne(filter: any): Promise<any>;
    create(item: T): Promise<T>;
    createMany(items: T[]): Promise<T[]>;
    update(id: string, item: T): import("mongoose").Query<import("mongoose").HydratedDocument<T, {}, {}>, import("mongoose").HydratedDocument<T, {}, {}>, {}, T>;
    aggregate(aggregateFilter: any): any;
}
