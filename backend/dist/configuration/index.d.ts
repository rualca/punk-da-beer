export declare const DATA_BASE_CONFIGURATION: {
    uri: string;
    user: string;
    pass: string;
    dbName: string;
};
export declare const AUTH_CONFIGURATION: {
    secret_key: string;
};
