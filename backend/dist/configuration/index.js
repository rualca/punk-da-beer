"use strict";
var _a, _b, _c, _d, _e;
Object.defineProperty(exports, "__esModule", { value: true });
exports.AUTH_CONFIGURATION = exports.DATA_BASE_CONFIGURATION = void 0;
exports.DATA_BASE_CONFIGURATION = {
    uri: (_a = process.env.MONGO_CONNECTION_STRING) !== null && _a !== void 0 ? _a : 'mongodb://localhost:27017',
    user: (_b = process.env.MONGO_USER) !== null && _b !== void 0 ? _b : 'root',
    pass: (_c = process.env.MONGO_PASSWORD) !== null && _c !== void 0 ? _c : '123',
    dbName: (_d = process.env.MONGO_DBNAME) !== null && _d !== void 0 ? _d : 'punkdabeer',
};
exports.AUTH_CONFIGURATION = {
    secret_key: (_e = process.env.SECRET_KEY) !== null && _e !== void 0 ? _e : 'shhIsASecret',
};
//# sourceMappingURL=index.js.map