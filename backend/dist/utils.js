"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.showLogo = void 0;
async function showLogo() {
    const logo = `      :*%%#-               **********+-.                %+                                
  :=--+#%%%%%*=.          .%%=:::::::=%%*               %+                                
=%%%-   :=#%%%%%%:        .%%:        =%%:     .=+++-   %+      :=+++=-         :=+++=:   
%%%%-      .=+%%%*        .%%:        #%#    +%%=-::-+*-%+    -#+-:::-=#=     +%+=:::-*%+ 
%%%%-          :++        .%%#*****##%+:    =%#        #%+   +%=       -%#   *%.       =%+
%%%%-         :+*+        .%%-.....::=%#+   *%-        .%+  .%%*=======+%%. :%*         %#
%%%%-      :*%%%%*        .%%:        .%%-  *%-        .%+  .%%-::::::::::  :%*         %%
:*%%-  :=*%%%%%#+.        .%%:        -%%-  =%#        *%+   *%=       .++   *%.       =%+
   :-+#%%%%%%+:           .%%+------=*%%+    *%%=...:+%=%+    =%+-.  .-##:    *%*-...:*%* 
`;
    console.log('\t');
    console.log(logo);
    console.log(`Okay people, it's time to go to the moon! 🚀`);
}
exports.showLogo = showLogo;
//# sourceMappingURL=utils.js.map