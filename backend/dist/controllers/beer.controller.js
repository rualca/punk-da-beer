"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BeerController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const dtos_1 = require("../core/dtos");
const services_1 = require("../services");
let BeerController = class BeerController {
    constructor(beerService) {
        this.beerService = beerService;
    }
    async getById(id) {
        return this.beerService.getBeerById(id);
    }
    async getByName(name) {
        return this.beerService.getBeerByName(name);
    }
    async get(filter) {
        return this.beerService.getBeers(filter);
    }
    async getAutocomplete(text) {
        return this.beerService.getBeersByNameOrIngredient(text);
    }
    async getTop10Ingredients() {
        return this.beerService.getTop10Ingredients();
    }
};
__decorate([
    (0, common_1.Get)(':id'),
    (0, swagger_1.ApiOperation)({ description: 'Get a beer by beer ID' }),
    (0, swagger_1.ApiResponse)({ status: 200, description: 'Return the beer found' }),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], BeerController.prototype, "getById", null);
__decorate([
    (0, common_1.Get)('/name/:name'),
    (0, swagger_1.ApiOperation)({ description: 'Get a beer by beer name' }),
    (0, swagger_1.ApiResponse)({ status: 200, description: 'Return the beer found' }),
    __param(0, (0, common_1.Param)('name')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], BeerController.prototype, "getByName", null);
__decorate([
    (0, common_1.Get)(''),
    (0, swagger_1.ApiOperation)({ description: 'Get a list of beers by a filter' }),
    (0, swagger_1.ApiResponse)({ status: 200, description: 'Return the beers found' }),
    __param(0, (0, common_1.Headers)('filter')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [dtos_1.GetBeerByFilterDto]),
    __metadata("design:returntype", Promise)
], BeerController.prototype, "get", null);
__decorate([
    (0, common_1.Get)('autocomplete/:text'),
    (0, swagger_1.ApiOperation)({ description: 'Get a list of beers and/or  by beer ID' }),
    (0, swagger_1.ApiResponse)({ status: 200, description: 'Return the beer found' }),
    __param(0, (0, common_1.Param)('text')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], BeerController.prototype, "getAutocomplete", null);
__decorate([
    (0, common_1.Get)('getTopTen'),
    (0, swagger_1.ApiOperation)({ description: 'Get a list of top ten ingredients' }),
    (0, swagger_1.ApiResponse)({ status: 200, description: 'Return the ingredients found' }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], BeerController.prototype, "getTop10Ingredients", null);
BeerController = __decorate([
    (0, swagger_1.ApiTags)('beers'),
    (0, common_1.Controller)('beer'),
    __metadata("design:paramtypes", [services_1.BeerService])
], BeerController);
exports.BeerController = BeerController;
//# sourceMappingURL=beer.controller.js.map