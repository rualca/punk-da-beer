import { BeerService } from 'src/services';
export declare class BeerController {
    private beerService;
    constructor(beerService: BeerService);
    getById(id: any): Promise<import("../core").Beer>;
}
