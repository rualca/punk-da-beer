import { AuthService } from '../frameworks/auth-service/passport/auth.service';
export declare class UserController {
    private authService;
    constructor(authService: AuthService);
    login(userData: any): Promise<{
        access_token: string;
    }>;
    getProfile(req: any): any;
}
