import { GetBeerByFilterDto } from '../core/dtos';
import { BeerService } from '../services';
export declare class BeerController {
    private beerService;
    constructor(beerService: BeerService);
    getById(id: string): Promise<import("../core").Beer>;
    getByName(name: string): Promise<import("../core").Beer>;
    get(filter: GetBeerByFilterDto): Promise<import("../core").Beer[]>;
    getAutocomplete(text: string): Promise<import("../core").Beer[]>;
    getTop10Ingredients(): Promise<any>;
}
