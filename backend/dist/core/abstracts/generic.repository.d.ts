export declare abstract class IGenericRepository<T> {
    abstract getAll(): Promise<T[]>;
    abstract count(): Promise<number>;
    abstract getById(id: string): Promise<T>;
    abstract get(filter: any): Promise<T[]>;
    abstract aggregate(aggregateFilter: any): any;
    abstract getOne(filter: any): Promise<T>;
    abstract create(item: T): Promise<T>;
    abstract createMany(item: T[]): Promise<T[]>;
    abstract update(id: string, item: T): any;
}
