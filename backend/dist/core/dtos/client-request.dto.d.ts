export declare class SaveClientRequestDto {
    method: string;
    url: string;
    headers: string[];
    body: any;
    query: any;
    params: any;
}
