export declare class GetBeerDto {
    name: string;
}
export declare class CreateBeerDto {
    name: string;
    tagline: string;
    first_brewed: string;
    description: string;
    image_url: string;
    abv: number;
    target_fg: number;
    target_og: number;
    ebc: number;
    srm: number;
    ph: number;
    attenuation_level: number;
    volume: {
        value: number;
        unit: string;
    };
    boil_volume: {
        value: number;
        unit: string;
    };
    method: {
        mash_temp: {
            temp: {
                value: number;
                unit: string;
            };
            duration: string;
        };
        fermentation: {
            temp: {
                value: number;
                unit: string;
            };
        };
        twist: string;
    };
    ingredients: {
        malt: {
            name: string;
            amount: {
                value: number;
                unit: string;
            };
        };
        hops: {
            name: string;
            amount: {
                value: number;
                unit: string;
            };
            add: string;
            attribute: string;
        }[];
        yeast: string;
    };
    food_pairing: string[];
    brewers_tips: string;
    contributed_by: string;
}
export declare class GetBeerAutocompleteDto {
    name: string;
    ingredient: string;
}
declare const UpdateBeerDto_base: import("@nestjs/common").Type<Partial<CreateBeerDto>>;
export declare class UpdateBeerDto extends UpdateBeerDto_base {
}
declare const GetBeerByFilterDto_base: import("@nestjs/common").Type<Partial<CreateBeerDto>>;
export declare class GetBeerByFilterDto extends GetBeerByFilterDto_base {
}
export {};
