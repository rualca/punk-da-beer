export declare class ClientRequest {
    method: string;
    url: string;
    headers: string[];
    body: any;
    query: any;
    params: any;
}
