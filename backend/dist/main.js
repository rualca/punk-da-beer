"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const core_1 = require("@nestjs/core");
const swagger_1 = require("@nestjs/swagger");
const app_module_1 = require("./app.module");
const services_1 = require("./services");
const utils_1 = require("./utils");
async function bootstrap() {
    var _a;
    await (0, utils_1.showLogo)();
    const app = await core_1.NestFactory.create(app_module_1.AppModule);
    const globalPrefix = 'api';
    app.setGlobalPrefix(globalPrefix);
    app.enableCors();
    const options = new swagger_1.DocumentBuilder()
        .setTitle('Punk Da Beer API')
        .setDescription('API Punk Da Beer')
        .setVersion('1.0')
        .addTag('API')
        .build();
    const document = swagger_1.SwaggerModule.createDocument(app, options, {
        include: [app_module_1.AppModule],
    });
    swagger_1.SwaggerModule.setup('api', app, document);
    const port = (_a = process.env.PORT) !== null && _a !== void 0 ? _a : 3001;
    await app.listen(port, () => {
        console.log('Listening at http://localhost:' + port + '/' + globalPrefix);
    });
    const beerService = app.get(services_1.BeerService);
    await beerService.initializeDatabase();
    await beerService.getTop10Ingredients();
}
bootstrap();
//# sourceMappingURL=main.js.map