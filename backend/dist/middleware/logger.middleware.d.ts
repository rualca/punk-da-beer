import { NestMiddleware } from '@nestjs/common';
import { Request, Response, NextFunction } from 'express';
import { ClientRequestService } from '../services/use-cases/client-request/client-request.service';
export declare class LoggerMiddleware implements NestMiddleware {
    private clientRequestService;
    constructor(clientRequestService: ClientRequestService);
    use(req: Request, res: Response, next: NextFunction): Promise<void>;
}
