import { IDataServices } from '../../../core/abstracts/data.service';
import { ClientRequest } from '../../../core/entities';
export declare class ClientRequestService {
    private dataServices;
    constructor(dataServices: IDataServices);
    saveClientRequest(clientRequestData: ClientRequest): Promise<ClientRequest>;
}
