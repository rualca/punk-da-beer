import { IDataServices } from '../../../core/abstracts/data.service';
import { User } from '../../../core/entities';
export declare class UserService {
    private dataServices;
    constructor(dataServices: IDataServices);
    getUserById(userId: string): Promise<User>;
    getUserByUsername(username: string): Promise<User>;
}
