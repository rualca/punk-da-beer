"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BeerServicesModule = void 0;
const axios_1 = require("@nestjs/axios");
const common_1 = require("@nestjs/common");
const data_service_1 = require("../../data-service/data.service");
const beer_service_1 = require("./beer.service");
let BeerServicesModule = class BeerServicesModule {
};
BeerServicesModule = __decorate([
    (0, common_1.Module)({
        imports: [data_service_1.DataServicesModule, axios_1.HttpModule],
        providers: [beer_service_1.BeerService],
        exports: [beer_service_1.BeerService],
    })
], BeerServicesModule);
exports.BeerServicesModule = BeerServicesModule;
//# sourceMappingURL=beer.module.js.map