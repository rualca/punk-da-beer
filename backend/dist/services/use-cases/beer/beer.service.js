"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BeerService = void 0;
const axios_1 = require("@nestjs/axios");
const common_1 = require("@nestjs/common");
const data_service_1 = require("../../../core/abstracts/data.service");
let BeerService = class BeerService {
    constructor(dataServices, httpService) {
        this.dataServices = dataServices;
        this.httpService = httpService;
    }
    async getBeerById(beerId) {
        const beer = await this.dataServices.beers.getById(beerId);
        return beer;
    }
    async getBeerByName(name) {
        const beer = await this.dataServices.beers.getOne({
            where: { name },
        });
        return beer;
    }
    async getBeersByNameOrIngredient(text) {
        const beers = await this.dataServices.beers.get({
            $or: [
                { name: { $regex: text, '$options': 'i' } },
                { 'ingredients.malt.name': { $regex: text, '$options': 'i' } },
                { 'ingredients.hops.name': { $regex: text, '$options': 'i' } },
                { 'ingredients.yeast': { $regex: text, '$options': 'i' } },
            ],
        });
        return beers;
    }
    async getBeers(filter) {
        const beers = await this.dataServices.beers.get(filter);
        return beers;
    }
    async getBeer(filter) {
        const beer = await this.dataServices.beers.getOne(filter);
        return beer;
    }
    async getTop10Ingredients() {
        const aggregateFilter = [
            {
                $project: {
                    name: 1,
                    'ingredients.malt.name': 1,
                    'ingredients.hops.name': 1,
                    'ingredients.yeast': [{ name: '$ingredients.yeast' }],
                },
            },
            {
                $project: {
                    allIngredients: {
                        $concatArrays: [
                            '$ingredients.malt',
                            '$ingredients.hops',
                            '$ingredients.yeast',
                        ],
                    },
                    name: 1,
                },
            },
            { $unwind: '$allIngredients' },
            {
                $group: {
                    _id: '$allIngredients.name',
                    count: { $sum: 1 },
                },
            },
            {
                $sort: { count: -1 },
            },
            {
                $limit: 10,
            },
        ];
        const top = await this.dataServices.beers.aggregate(aggregateFilter);
        return top;
    }
    async insertBeers(beers) {
        const result = await this.dataServices.beers.createMany(beers);
        if (result.length > 0) {
            console.info('Inserted!');
            return true;
        }
    }
    async countBeers() {
        return this.dataServices.beers.count();
    }
    async initializeDatabase() {
        console.info('Initializing database...');
        console.info('Checking if the database have beers already 🍺...');
        const totalBeers = await this.countBeers();
        console.info(`${totalBeers} beers encountered.`);
        if (totalBeers > 0) {
            return;
        }
        console.info(`Inserting beers of Punk API...`);
        const url = 'https://api.punkapi.com/v2/beers?page=1&per_page=80';
        this.httpService
            .get(url)
            .subscribe((response) => this.insertBeers(response.data));
    }
};
BeerService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [data_service_1.IDataServices,
        axios_1.HttpService])
], BeerService);
exports.BeerService = BeerService;
//# sourceMappingURL=beer.service.js.map