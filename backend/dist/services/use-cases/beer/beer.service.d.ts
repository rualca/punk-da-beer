import { HttpService } from '@nestjs/axios';
import { IDataServices } from '../../../core/abstracts/data.service';
import { GetBeerByFilterDto } from '../../../core/dtos';
import { Beer } from '../../../core/entities';
export declare class BeerService {
    private dataServices;
    private httpService;
    constructor(dataServices: IDataServices, httpService: HttpService);
    getBeerById(beerId: string): Promise<Beer>;
    getBeerByName(name: string): Promise<Beer>;
    getBeersByNameOrIngredient(text: string): Promise<Beer[]>;
    getBeers(filter: GetBeerByFilterDto): Promise<Beer[]>;
    getBeer(filter: GetBeerByFilterDto): Promise<Beer>;
    getTop10Ingredients(): Promise<any>;
    private insertBeers;
    private countBeers;
    initializeDatabase(): Promise<void>;
}
