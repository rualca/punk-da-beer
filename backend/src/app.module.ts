import {
  MiddlewareConsumer,
  Module,
  NestModule,
  RequestMethod,
} from '@nestjs/common';
import { APP_GUARD } from '@nestjs/core';
import { BeerController } from './controllers';
import { UserController } from './controllers/user.controller';
import { AuthModule } from './frameworks/auth-service/passport/auth.module';
import { JwtAuthGuard } from './frameworks/auth-service/passport/jwt-auth.guard';
import { LoggerMiddleware } from './middleware/logger.middleware';
import { DataServicesModule } from './services/data-service/data.service';
import { BeerServicesModule } from './services/use-cases/beer/beer.module';
import { ClientServicesModule } from './services/use-cases/client-request/client-request.module';

@Module({
  imports: [
    DataServicesModule,
    BeerServicesModule,
    AuthModule,
    ClientServicesModule,
  ],
  controllers: [BeerController, UserController],
  providers: [
    {
      provide: APP_GUARD,
      useExisting: JwtAuthGuard,
    },
    JwtAuthGuard
  ],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(LoggerMiddleware)
      .forRoutes({ path: '*', method: RequestMethod.ALL });
  }
}
