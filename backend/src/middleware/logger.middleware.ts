import { Injectable, NestMiddleware } from '@nestjs/common';
import { Request, Response, NextFunction } from 'express';
import { SaveClientRequestDto } from '../core/dtos/client-request.dto';
import { ClientRequestService } from '../services/use-cases/client-request/client-request.service';

@Injectable()
export class LoggerMiddleware implements NestMiddleware {
  constructor(private clientRequestService: ClientRequestService) {}

  async use(req: Request, res: Response, next: NextFunction) {
    const requestToInsert: SaveClientRequestDto = {
      method: req.method,
      url: req.url,
      headers: req.rawHeaders,
      body: req.body,
      query: req.query,
      params: req.params,
    };

    await this.clientRequestService.saveClientRequest(requestToInsert);

    next();
  }
}
