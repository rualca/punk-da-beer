import * as request from 'supertest';
import { Test } from '@nestjs/testing';
import { CanActivate, ExecutionContext, HttpStatus, INestApplication } from '@nestjs/common';
import { AuthService } from '../frameworks/auth-service/passport/auth.service';
import { AppModule } from '../app.module';
import { JwtAuthGuard } from '../frameworks/auth-service/passport/jwt-auth.guard';

describe('User', () => {
  let app: INestApplication;
  let accessToken: string;

  beforeAll(async () => {
    const mock_ForceFailGuard: CanActivate = {
      canActivate: (context: ExecutionContext) => {
        const req = context.switchToHttp().getRequest();

        req.user = {username: 'ruben', id: '123'};

        return true;
      },
    };
    const moduleRef = await Test.createTestingModule({
      imports: [AppModule],
      providers: [
        {
          provide: AuthService,
          useValue: {
            login: () => {
              return {access_token: '123'}
            } 
          }
        },
      ]
    })
    .overrideProvider(JwtAuthGuard).useValue(mock_ForceFailGuard)
    .compile();

    app = moduleRef.createNestApplication();
    await app.init();
  });

  it(`/POST login`, async () => {
    const response = await request(app.getHttpServer())
      .post('/auth/login');

    accessToken = response.body.access_token;

    expect(response.status).toBe(HttpStatus.CREATED);
  });

  it(`/Get profile`, () => {
    return request(app.getHttpServer())
      .get('/user/profile')
      .set('Authorization', `Bearer ${accessToken}`)
      .expect(200)
      .expect({
        username: 'ruben',
        id: '123'
      });
  });

  afterAll(async () => {
    await app.close();
  });
});