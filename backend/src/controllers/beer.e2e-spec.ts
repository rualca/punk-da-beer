import * as request from 'supertest';
import { Test } from '@nestjs/testing';
import { CanActivate, ExecutionContext, INestApplication } from '@nestjs/common';
import { AuthService } from '../frameworks/auth-service/passport/auth.service';
import { AppModule } from '../app.module';
import { Beer } from '../core/entities';
import { JwtAuthGuard } from '../frameworks/auth-service/passport/jwt-auth.guard';
import { BeerService } from '../services';
import mongoose from 'mongoose';

describe('Beer', () => {
  let app: INestApplication;

  const beerMock =
  {
    "_id" : "625718080df1e6edf6b4ff79",
    "id" : "1",
    "name" : "Buzz",
    "tagline" : "A Real Bitter Experience.",
    "first_brewed" : "09/2007",
    "description" : "A light, crisp and bitter IPA brewed with English and American hops. A small batch brewed only once.",
    "image_url" : "https://images.punkapi.com/v2/keg.png",
    "abv" : 4.5,
    "target_fg" : 1010,
    "target_og" : 1044,
    "ebc" : 20,
    "srm" : 10,
    "ph" : 4.4,
    "attenuation_level" : 75,
    "volume" : {
      "value" : 20,
      "unit" : "litres"
    },
    "boil_volume" : {
      "value" : 25,
      "unit" : "litres"
    },
    "method" : {
      "mash_temp" : [
        {
          "temp" : {
            "value" : 64,
            "unit" : "celsius"
          },
          "duration" : 75
        }
      ],
      "fermentation" : {
        "temp" : {
          "value" : 19,
          "unit" : "celsius"
        }
      },
      "twist" : null
    },
    "ingredients" : {
      "malt" : [
        {
          "name" : "Maris Otter Extra Pale",
          "amount" : {
            "value" : 3.3,
            "unit" : "kilograms"
          }
        },
        {
          "name" : "Caramalt",
          "amount" : {
            "value" : 0.2,
            "unit" : "kilograms"
          }
        },
        {
          "name" : "Munich",
          "amount" : {
            "value" : 0.4,
            "unit" : "kilograms"
          }
        }
      ],
      "hops" : [
        {
          "name" : "Fuggles",
          "amount" : {
            "value" : 25,
            "unit" : "grams"
          },
          "add" : "start",
          "attribute" : "bitter"
        },
        {
          "name" : "First Gold",
          "amount" : {
            "value" : 25,
            "unit" : "grams"
          },
          "add" : "start",
          "attribute" : "bitter"
        },
        {
          "name" : "Fuggles",
          "amount" : {
            "value" : 37.5,
            "unit" : "grams"
          },
          "add" : "middle",
          "attribute" : "flavour"
        },
        {
          "name" : "First Gold",
          "amount" : {
            "value" : 37.5,
            "unit" : "grams"
          },
          "add" : "middle",
          "attribute" : "flavour"
        },
        {
          "name" : "Cascade",
          "amount" : {
            "value" : 37.5,
            "unit" : "grams"
          },
          "add" : "end",
          "attribute" : "flavour"
        }
      ],
      "yeast" : "Wyeast 1056 - American Ale™"
    },
    "food_pairing" : [ "Spicy chicken tikka masala", "Grilled chicken quesadilla", "Caramel toffee cake" ],
    "brewers_tips" : "The earthy and floral aromas from the hops can be overpowering. Drop a little Cascade in at the end of the boil to lift the profile with a bit of citrus.",
    "contributed_by" : "Sam Mason <samjbmason>",
    "__v" : 0
  }

  beforeAll(async () => {
    const mock_ForceFailGuard: CanActivate = {
      canActivate: (context: ExecutionContext) => {
        const req = context.switchToHttp().getRequest();

        req.user = {username: 'ruben', id: '123'};

        return true;
      },
    };

    const moduleRef = await Test.createTestingModule({
      imports: [AppModule],
    })
    .overrideProvider(JwtAuthGuard).useValue(mock_ForceFailGuard)
    .compile();

    app = moduleRef.createNestApplication();
    await app.init();
  });

  it(`/GET getById`, () => {
    return request(app.getHttpServer())
      .get(`/beer/${beerMock._id}`)
      .expect(200)
      .expect(beerMock);
  });

  it(`/GET getByName`, () => {
    return request(app.getHttpServer())
      .get('/beer/name/Buzz')
      .expect(200)
      .expect(beerMock);
  });

  it(`/GET get`, () => {
    return request(app.getHttpServer())
      .get('/beer/') //filter en header
      .set('filter', JSON.stringify({
        name: 'Buzz'
      }))
      .expect(200)
      .expect([beerMock]);
  });

  it(`/GET getAutocomplete`, () => {
    return request(app.getHttpServer())
      .get('/beer/autocomplete/Buzz') //filter en header
      .expect(200)
      .expect([beerMock]);
  });

  afterAll(async () => {
    await app.close();
  });
});