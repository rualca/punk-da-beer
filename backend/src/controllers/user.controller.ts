import {
  Controller,
  Get,
  Param,
  Post,
  Body,
  Put,
  UseGuards,
  Request,
} from '@nestjs/common';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { AuthService } from '../frameworks/auth-service/passport/auth.service';
import { JwtAuthGuard } from '../frameworks/auth-service/passport/jwt-auth.guard';
import { Public } from '../frameworks/auth-service/passport/public.decorator';

@ApiTags('auth')
@Controller()
export class UserController {
  constructor(private authService: AuthService) {}

  @Public()
  @Post('auth/login')
  @ApiOperation({ description: 'Login the user credentials' })
  @ApiResponse({ status: 200, description: 'Return the user access token'})
  async login(@Body() userData) {
    return this.authService.login(userData);
  }

  @Get('user/profile')
  @ApiOperation({ description: 'Decrypt user token' })
  @ApiResponse({ status: 200, description: 'Return the user profile data'})
  getProfile(@Request() req) {
    return req.user;
  }
}
