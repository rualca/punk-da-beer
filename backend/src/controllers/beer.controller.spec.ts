import { Test } from '@nestjs/testing';
import { Beer } from '../core/entities';
import { BeerService } from '../services';
import { BeerController } from './beer.controller';

describe('BeerController', () => {
  let beerController: BeerController;
  let beerService: BeerService;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      controllers: [BeerController],
      providers: [{
        provide: BeerService,
        useValue: {
          getBeerById: jest.fn(),
          getBeerByName: jest.fn(),
          getBeers: jest.fn(),
          getBeersByNameOrIngredient: jest.fn(),
          getTop10Ingredients: jest.fn(),
        }
      }],
    }).compile();

    beerService = moduleRef.get<BeerService>(BeerService);
    beerController = moduleRef.get<BeerController>(BeerController);
  });

  describe('getById', () => {
    it('should return an object with Beer data found by id', async () => {
      const result = new Promise<Beer>((resolve, reject) => {
        resolve(new Beer());
      });
      const id = '123';
      
      jest.spyOn(beerService, 'getBeerById').mockImplementation(() => result);

      const testResult = await beerController.getById(id);

      expect(testResult).toBe(await result);
      expect(beerService.getBeerById).toHaveBeenCalled();
    });
  });

  describe('getByName', () => {
    it('should return an object with Beer data found by name', async () => {
      const result = new Promise<Beer>((resolve, reject) => {
        resolve(new Beer());
      });
      const name = 'Buzz';

      jest.spyOn(beerService, 'getBeerByName').mockImplementation(() => result);

      const testResult = await beerController.getByName(name);

      expect(testResult).toBe(await result);
      expect(beerService.getBeerByName).toHaveBeenCalled();
    });
  });

  describe('get', () => {
    it('should return an arrays with objects with Beer data', async () => {
      const result = new Promise<Beer[]>((resolve, reject) => {
        resolve([new Beer(), new Beer()]);
      });

      const filter = {
          name: 'Buzz',
          ingredient: '123'
      };

      jest.spyOn(beerService, 'getBeers').mockImplementation(() => result);

      const testResult = await beerController.get(filter);

      expect(testResult).toBe(await result);
      expect(beerService.getBeers).toHaveBeenCalled();
    });
  });

  describe('getAutocomplete', () => {
    it('should return an array with objects with Beer or Ingredient data', async () => {
      const result = new Promise<Beer[]>((resolve, reject) => {
        resolve([new Beer(), new Beer()]);
      });
      const text = 'buzz';

      jest.spyOn(beerService, 'getBeersByNameOrIngredient').mockImplementation(() => result);

      const testResult = await beerController.getAutocomplete(text);

      expect(testResult).toBe(await result);
      expect(beerService.getBeersByNameOrIngredient).toHaveBeenCalled();
    });
  });

  describe('getTop10Ingredients', () => {
    it('should return an array with objects with ingredients data', async () => {
      const result = new Promise<Beer[]>((resolve, reject) => {
        resolve([new Beer(), new Beer()]);
      });

      jest.spyOn(beerService, 'getTop10Ingredients').mockImplementation(() => result);

      const testResult = await beerController.getTop10Ingredients();

      expect(testResult).toBe(await result);
      expect(beerService.getTop10Ingredients).toHaveBeenCalled();
    });
  });
});