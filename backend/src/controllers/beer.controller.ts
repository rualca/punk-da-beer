import { Controller, Get, Param, Headers } from '@nestjs/common';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { GetBeerAutocompleteDto, GetBeerByFilterDto } from '../core/dtos';
import { BeerService } from '../services';

@ApiTags('beers')
@Controller('beer')
export class BeerController {
  constructor(private beerService: BeerService) {}

  // A private route to get a beer, by ID
  @Get(':id')
  @ApiOperation({ description: 'Get a beer by beer ID' })
  @ApiResponse({ status: 200, description: 'Return the beer found'})
  async getById(@Param('id') id: string) {
    return this.beerService.getBeerById(id);
  }

  // A private route to get a beer, by Name
  @Get('/name/:name')
  @ApiOperation({ description: 'Get a beer by beer name' })
  @ApiResponse({ status: 200, description: 'Return the beer found'})
  async getByName(@Param('name') name: string) {
    return this.beerService.getBeerByName(name);
  }

  // A private route to get a beer based on main filters(name, ingredients)
  @Get('')
  @ApiOperation({ description: 'Get a list of beers by a filter' })
  @ApiResponse({ status: 200, description: 'Return the beers found'})
  async get(@Headers('filter') filter: GetBeerByFilterDto) {
    return this.beerService.getBeers(filter);
  }

  // A private route to autocomplete beer name or ingredient
  @Get('autocomplete/:text')
  @ApiOperation({ description: 'Get a list of beers and/or  by beer ID' })
  @ApiResponse({ status: 200, description: 'Return the beer found'})
  async getAutocomplete(@Param('text') text: string) {
    return this.beerService.getBeersByNameOrIngredient(text);
  }

  // A private route to get the top 10 ingredients more repeated
  @Get('getTopTen')
  @ApiOperation({ description: 'Get a list of top ten ingredients' })
  @ApiResponse({ status: 200, description: 'Return the ingredients found'})
  async getTop10Ingredients() {
    return this.beerService.getTop10Ingredients();
  }
}
