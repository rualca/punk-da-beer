import { Test } from '@nestjs/testing';
import { AuthService } from '../frameworks/auth-service/passport/auth.service';
import { UserController } from './user.controller';

describe('UserController', () => {
  let userController: UserController;
  let authService: AuthService;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
        controllers: [UserController],
        providers: [{
          provide: AuthService,
          useValue: {
            login: jest.fn()
          }
        }],
      }).compile();

    authService = moduleRef.get<AuthService>(AuthService);
    userController = moduleRef.get<UserController>(UserController);
  });

  describe('login', () => {
    it('should return an object with access_token', async () => {
      const result = new Promise<{access_token: string}>((resolve, reject) => {
        resolve({access_token: '123'});
      });
      const userData = {
          username: 'ruben',
          password: '123'
      };

      jest.spyOn(authService, 'login').mockImplementation(() => result);

      const testResult = await userController.login(userData);

      expect(testResult).toBe(await result);
    });
  });

  describe('getProfile', () => {
    it('should return an object with user profile', async () => {
      const result = { userId: '123', username: 'ruben' };
      const req: any = {user: { userId: '123', username: 'ruben' }};

      const testResult = await userController.getProfile(req);

      expect(testResult).toStrictEqual(result);
    });
  });
});