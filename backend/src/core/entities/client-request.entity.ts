export class ClientRequest {
  // id: string;

  method: string;

  url: string;

  headers: string[];

  body: any;

  query: any;

  params: any;
}
