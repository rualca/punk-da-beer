import { ApiProperty, PartialType } from '@nestjs/swagger';
import { IsString, IsNotEmpty } from 'class-validator';

export class GetBeerDto {
  @IsString()
  @IsNotEmpty()
  name: string;
}

export class CreateBeerDto {
  @ApiProperty()
  name: string;

  // id: string;

  @ApiProperty()
  tagline: string;

  @ApiProperty()
  first_brewed: string;

  @ApiProperty()
  description: string;

  @ApiProperty()
  image_url: string;

  @ApiProperty()
  abv: number;

  @ApiProperty()
  target_fg: number;

  @ApiProperty()
  target_og: number;

  @ApiProperty()
  ebc: number;

  @ApiProperty()
  srm: number;

  @ApiProperty()
  ph: number;

  @ApiProperty()
  attenuation_level: number;

  @ApiProperty()
  volume: {
    value: number;
    unit: string;
  };

  @ApiProperty()
  boil_volume: {
    value: number;
    unit: string;
  };

  @ApiProperty()
  method: {
    mash_temp: {
      temp: {
        value: number;
        unit: string;
      };
      duration: string;
    };
    fermentation: {
      temp: {
        value: number;
        unit: string;
      };
    };
    twist: string;
  };

  @ApiProperty()
  ingredients: {
    malt: {
      name: string;
      amount: {
        value: number;
        unit: string;
      };
    };
    hops: {
      name: string;
      amount: {
        value: number;
        unit: string;
      };
      add: string;
      attribute: string;
    }[];
    yeast: string;
  };

  @ApiProperty()
  food_pairing: string[];

  @ApiProperty()
  brewers_tips: string;

  @ApiProperty()
  contributed_by: string;
}

export class GetBeerAutocompleteDto {
  @ApiProperty()
  name: string;

  @ApiProperty()
  ingredient: string;
}

export class UpdateBeerDto extends PartialType(CreateBeerDto) {}
export class GetBeerByFilterDto extends PartialType(CreateBeerDto) {}
