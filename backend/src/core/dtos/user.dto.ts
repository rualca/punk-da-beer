import { IsString, IsNotEmpty } from 'class-validator';

export class GetUserDto {
  @IsString()
  @IsNotEmpty()
  username: string;
}

export class LoginUserDto {
  @IsString()
  @IsNotEmpty()
  password: string;
}
