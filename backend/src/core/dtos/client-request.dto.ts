import { ApiProperty } from '@nestjs/swagger';

export class SaveClientRequestDto {
  @ApiProperty()
  method: string;

  @ApiProperty()
  url: string;

  @ApiProperty()
  headers: string[];

  @ApiProperty()
  body: any;

  @ApiProperty()
  query: any;

  @ApiProperty()
  params: any;
}
