import { ClientRequest, User } from '../entities';
import { Beer } from '../entities/beer.entity';
import { IGenericRepository } from './generic.repository';

export abstract class IDataServices {
  abstract beers: IGenericRepository<Beer>;
  abstract users: IGenericRepository<User>;
  abstract clientRequests: IGenericRepository<ClientRequest>;
}
