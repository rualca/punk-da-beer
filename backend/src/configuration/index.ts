export const DATA_BASE_CONFIGURATION = {
  uri: process.env.MONGO_CONNECTION_STRING ?? 'mongodb://localhost:27017',
  user: process.env.MONGO_USER ?? 'root',
  pass: process.env.MONGO_PASSWORD ?? '123',
  dbName: process.env.MONGO_DBNAME ?? 'punkdabeer',
};

export const AUTH_CONFIGURATION = {
  secret_key: process.env.SECRET_KEY ?? 'shhIsASecret',
}

