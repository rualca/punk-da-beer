import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { AppModule } from './app.module';
import { BeerService } from './services';
import { showLogo } from './utils';

async function bootstrap() {
  await showLogo();

  const app = await NestFactory.create(AppModule);

  const globalPrefix = 'api';
  app.setGlobalPrefix(globalPrefix);
  app.enableCors();

  // ╔═╗╦ ╦╔═╗╔═╗╔═╗╔═╗╦═╗
  // ╚═╗║║║╠═╣║ ╦║ ╦║╣ ╠╦╝
  // ╚═╝╚╩╝╩ ╩╚═╝╚═╝╚═╝╩╚═
  const options = new DocumentBuilder()
    .setTitle('Punk Da Beer API')
    .setDescription('API Punk Da Beer')
    .setVersion('1.0')
    .addTag('API')
    .build();

  const document = SwaggerModule.createDocument(app, options, {
    include: [AppModule],
  });

  SwaggerModule.setup('api', app, document);

  const port = process.env.PORT ?? 3001;

  await app.listen(port, () => {
    console.log('Listening at http://localhost:' + port + '/' + globalPrefix);
  });

  const beerService = app.get(BeerService);

  await beerService.initializeDatabase();

  ////////////////////////////////
  await beerService.getTop10Ingredients();
}

bootstrap();
