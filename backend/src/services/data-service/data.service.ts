import { Module } from '@nestjs/common';
import { MongoDataServicesModule } from '../../frameworks/data-service/mongo/mongo-data.module';

@Module({
  imports: [MongoDataServicesModule],
  exports: [MongoDataServicesModule],
})
export class DataServicesModule {}
