import { Module } from '@nestjs/common';
import { DataServicesModule } from '../../data-service/data.service';
import { UserService } from './user.service';

@Module({
  imports: [DataServicesModule],
  providers: [UserService],
  exports: [UserService],
})
export class UserServicesModule {}
