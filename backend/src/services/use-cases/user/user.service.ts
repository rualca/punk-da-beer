import { Injectable } from '@nestjs/common';
import { IDataServices } from '../../../core/abstracts/data.service';
import { User } from '../../../core/entities';

@Injectable()
export class UserService {
  constructor(private dataServices: IDataServices) {}

  /**
   * This function returns a promise that resolves to a user object.
   * @param {string} userId - string - This is the userId that we're going to use to get the user from
   * the database.
   * @returns A promise of a user
   */
  async getUserById(userId: string): Promise<User> {
    const user = await this.dataServices.users.getById(userId);

    return user;
  }

  /**
   * It returns a user object from the database, based on the username
   * @param {string} username - string - This is the username that we're going to use to find the user.
   * @returns A user object
   */
  async getUserByUsername(username: string): Promise<User> {
    const user = await this.dataServices.users.getOne({
      where: { username },
    });

    return user;
  }
}
