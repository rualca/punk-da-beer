import { HttpModule } from '@nestjs/axios';
import { Module } from '@nestjs/common';
import { DataServicesModule } from '../../data-service/data.service';
import { BeerService } from './beer.service';

@Module({
  imports: [DataServicesModule, HttpModule],
  providers: [BeerService],
  exports: [BeerService],
})
export class BeerServicesModule {}
