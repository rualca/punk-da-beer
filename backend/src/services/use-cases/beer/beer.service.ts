import { HttpService } from '@nestjs/axios';
import { Injectable } from '@nestjs/common';
import { IDataServices } from '../../../core/abstracts/data.service';
import { GetBeerByFilterDto } from '../../../core/dtos';
import { Beer } from '../../../core/entities';

@Injectable()
export class BeerService {
  constructor(
    private dataServices: IDataServices,
    private httpService: HttpService,
  ) {}

  /**
   * It returns a promise that resolves to a beer
   * @param {string} beerId - string - The ID of the beer to retrieve.
   * @returns A beer object
   */
  async getBeerById(beerId: string): Promise<Beer> {
    const beer = await this.dataServices.beers.getById(beerId);

    return beer;
  }

  /**
   * It returns a promise that resolves to a beer object
   * @param {string} name - string - The name of the beer we want to get
   * @returns A beer object
   */
  async getBeerByName(name: string): Promise<Beer> {
    const beer = await this.dataServices.beers.getOne({
      where: { name },
    });

    return beer;
  }

  /**
   * It gets all the beers that have a name or ingredient that matches the text
   * @param {string} text - string - The text to search for.
   * @returns An array of beers that match the search criteria.
   */
  async getBeersByNameOrIngredient(text: string): Promise<Beer[]> {
    const beers = await this.dataServices.beers.get({
      $or: [
        { name: { $regex: text, '$options' : 'i' } },
        { 'ingredients.malt.name': { $regex: text, '$options' : 'i' } },
        { 'ingredients.hops.name': { $regex: text, '$options' : 'i' } },
        { 'ingredients.yeast': { $regex: text, '$options' : 'i' } },
      ],
    });

    return beers;
  }

  /**
   * It takes a filter object, and returns an array of beers
   * @param {GetBeerByFilterDto} filter - GetBeerByFilterDto
   * @returns An array of beers
   */
  async getBeers(filter: GetBeerByFilterDto): Promise<Beer[]> {
    const beers = await this.dataServices.beers.get(filter);

    return beers;
  }

  /**
   * It takes a filter object, passes it to the data service, and returns the result
   * @param {GetBeerByFilterDto} filter - GetBeerByFilterDto
   * @returns A beer object
   */
  async getBeer(filter: GetBeerByFilterDto): Promise<Beer> {
    const beer = await this.dataServices.beers.getOne(filter);

    return beer;
  }

  /**
   * We're using the aggregate function to filter the beers collection, and then we're returning the
   * top 10 ingredients
   * @returns The top 10 ingredients used in beers.
   */
  async getTop10Ingredients() {
    const aggregateFilter = [
      {
        $project: {
          name: 1,
          'ingredients.malt.name': 1,
          'ingredients.hops.name': 1,
          'ingredients.yeast': [{ name: '$ingredients.yeast' }],
        },
      },
      {
        $project: {
          allIngredients: {
            $concatArrays: [
              '$ingredients.malt',
              '$ingredients.hops',
              '$ingredients.yeast',
            ],
          },
          name: 1,
        },
      },
      { $unwind: '$allIngredients' },
      {
        $group: {
          _id: '$allIngredients.name',
          count: { $sum: 1 },
        },
      },
      {
        $sort: { count: -1 },
      },
      {
        $limit: 10,
      },
    ];

    const top = await this.dataServices.beers.aggregate(aggregateFilter);

    return top;
  }

  /**
   * It takes an array of beers, inserts them into the database, and returns a boolean value indicating
   * whether the operation was successful
   * @param {Beer[]} beers - Beer[] - an array of Beer objects
   * @returns A boolean value.
   */
  private async insertBeers(beers: Beer[]): Promise<boolean> {
    const result = await this.dataServices.beers.createMany(beers);

    if (result.length > 0) {
      console.info('Inserted!');

      return true;
    }
  }

  /**
   * "This function returns a promise that resolves to the number of beers in the database."
   * 
   * The async keyword tells TypeScript that this function returns a promise. The await keyword tells
   * TypeScript that the function will wait for the promise to resolve before continuing
   * @returns A promise that resolves to the number of beers in the database.
   */
  private async countBeers(): Promise<number> {
    return this.dataServices.beers.count();
  }

  /**
   * It checks if the database has beers, if it doesn't, it fetches beers from Punk API and inserts
   * them into the database
   * @returns the number of beers in the database.
   */
  async initializeDatabase() {
    console.info('Initializing database...');
    console.info('Checking if the database have beers already 🍺...');

    const totalBeers = await this.countBeers();

    console.info(`${totalBeers} beers encountered.`);

    if (totalBeers > 0) {
      return;
    }

    console.info(`Inserting beers of Punk API...`);

    const url = 'https://api.punkapi.com/v2/beers?page=1&per_page=80';

    this.httpService
      .get(url)
      .subscribe((response) => this.insertBeers(response.data));
  }
}
