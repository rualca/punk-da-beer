import { Injectable } from '@nestjs/common';
import { IDataServices } from '../../../core/abstracts/data.service';
import { ClientRequest } from '../../../core/entities';

@Injectable()
export class ClientRequestService {
  constructor(private dataServices: IDataServices) {}

  /**
   * It creates a new client request in the database
   * @param {ClientRequest} clientRequestData - This is the data that we want to save to the database.
   * @returns A promise that resolves to a ClientRequest object.
   */
  async saveClientRequest(
    clientRequestData: ClientRequest,
  ): Promise<ClientRequest> {
    const clientRequest = await this.dataServices.clientRequests.create(
      clientRequestData,
    );

    return clientRequest;
  }
}
