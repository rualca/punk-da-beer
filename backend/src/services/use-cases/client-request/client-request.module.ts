import { Module } from '@nestjs/common';
import { DataServicesModule } from '../../data-service/data.service';
import { ClientRequestService } from './client-request.service';

@Module({
  imports: [DataServicesModule],
  providers: [ClientRequestService],
  exports: [ClientRequestService],
})
export class ClientServicesModule {}
