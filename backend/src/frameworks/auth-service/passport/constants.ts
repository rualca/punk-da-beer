import { AUTH_CONFIGURATION } from '../../../configuration';

export const jwtConstants = {
  secret: AUTH_CONFIGURATION.secret_key,
};
