import { Test } from "@nestjs/testing";
import { UserService } from "../../../services";
import { AuthService } from "./auth.service";
import { User } from '../../../core/entities';
import { JwtService } from "@nestjs/jwt";


describe('AuthService', () => {
  let userService: UserService;
  let authService: AuthService;
  let jwtService: JwtService;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
        providers: [
          AuthService,
          {
            provide: UserService,
            useValue: {
              getUserByUsername: jest.fn()
            }
          },
          {
            provide: JwtService,
            useValue: {
              sign: jest.fn()
            }
          },

        ],
      }).compile();

    authService = moduleRef.get<AuthService>(AuthService);
    userService = moduleRef.get<UserService>(UserService);
    jwtService = moduleRef.get<JwtService>(JwtService);
  });

  describe('validateUser', () => {
    it('should return user object', async () => {
      const result = {
        id: '123',
        username: 'ruben',
      };
      const getUserByUsernameResultMock = new Promise<User>((resolve, reject) => resolve({
        id: '123',
        username: 'ruben',
        password: '123'
      }));
      const userData = { username: 'ruben', password: '123' };

      jest.spyOn(userService, 'getUserByUsername').mockImplementation(() => getUserByUsernameResultMock);

      const testResult = await authService.validateUser(userData.username, userData.password);

      expect(testResult).toStrictEqual(result);
    });
  });

  describe('login', () => {
    it('should return user access token', async () => {
      const result = {
        access_token: '1234'
      };

      const userData = { username: 'ruben', id: '123' };

      jest.spyOn(jwtService, 'sign').mockImplementation(() => '1234');

      const testResult = await authService.login(userData);

      expect(testResult).toStrictEqual(result);
    });
  });
})