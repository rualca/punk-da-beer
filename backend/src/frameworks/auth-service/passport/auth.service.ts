import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UserService } from '../../../services';

@Injectable()
export class AuthService {
  constructor(
    private usersService: UserService,
    private jwtService: JwtService,
  ) {}

  /**
   * It takes a username and password, gets the user from the database, and if the password matches,
   * returns the user object without the password
   * @param {string} username - string - The username of the user to validate
   * @param {string} pass - string - The password that the user entered
   * @returns The user object without the password.
   */
  async validateUser(username: string, pass: string): Promise<any> {
    const user = await this.usersService.getUserByUsername(username);

    if (user && user.password === pass) {
      const { password, ...result } = user;

      return result;
    }

    return null;
  }

  /**
   * It takes a user object, creates a payload object, and then returns an object with an access_token
   * property that contains the JWT
   * @param {any} user - any - The user object that is passed in from the controller.
   * @returns The access token is being returned.
   */
  async login(user: any): Promise<{
    access_token: string;
  }> {
    const payload = { username: user.username, sub: user.id };

    return {
      access_token: this.jwtService.sign(payload),
    };
  }
}
