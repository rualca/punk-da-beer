import { Injectable, OnApplicationBootstrap } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { IDataServices } from '../../../core';
import { Beer, BeerDocument, User, UserDocument } from './model';
import {
  ClientRequest,
  ClientRequestDocument,
} from './model/client-request.model';
import { MongoGenericRepository } from './mongo-generic.repository';

@Injectable()
export class MongoDataServices
  implements IDataServices, OnApplicationBootstrap
{
  beers: MongoGenericRepository<Beer>;
  users: MongoGenericRepository<User>;
  clientRequests: MongoGenericRepository<ClientRequest>;

  constructor(
    @InjectModel(Beer.name)
    private beerRepository: Model<BeerDocument>,
    @InjectModel(User.name)
    private userRepository: Model<UserDocument>,
    @InjectModel(ClientRequest.name)
    private clientRequestRepository: Model<ClientRequestDocument>,
  ) {}

  onApplicationBootstrap() {
    this.beers = new MongoGenericRepository<Beer>(this.beerRepository, [
      'name',
    ]);

    this.users = new MongoGenericRepository<User>(this.userRepository, [
      'username',
    ]);

    this.clientRequests = new MongoGenericRepository<ClientRequest>(
      this.clientRequestRepository,
      ['id'],
    );
  }
}
