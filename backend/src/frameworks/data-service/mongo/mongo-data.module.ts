import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { IDataServices } from '../../../core';
import { DATA_BASE_CONFIGURATION } from '../../../configuration';
import {
  Beer,
  BeerSchema,
  ClientRequest,
  ClientRequestSchema,
  User,
  UserSchema,
} from './model';
import { MongoDataServices } from './mongo-data.service';

@Module({
  imports: [
    MongooseModule.forRoot(DATA_BASE_CONFIGURATION.uri, {
      useNewUrlParser: true,
      user: DATA_BASE_CONFIGURATION.user,
      pass: DATA_BASE_CONFIGURATION.pass,
      dbName: DATA_BASE_CONFIGURATION.dbName,
    }),
    MongooseModule.forFeature([{ name: Beer.name, schema: BeerSchema }]),
    MongooseModule.forFeature([{ name: User.name, schema: UserSchema }]),
    MongooseModule.forFeature([
      { name: ClientRequest.name, schema: ClientRequestSchema },
    ]),
  ],
  providers: [
    {
      provide: IDataServices,
      useClass: MongoDataServices,
    },
  ],
  exports: [IDataServices],
})
export class MongoDataServicesModule {}
