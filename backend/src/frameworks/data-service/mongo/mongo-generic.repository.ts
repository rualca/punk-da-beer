import { Model } from 'mongoose';
import { IGenericRepository } from '../../../core';

export class MongoGenericRepository<T> implements IGenericRepository<T> {
  private _repository: Model<T>;
  private _populateOnFind: string[];

  constructor(repository: Model<T>, populateOnFind: string[] = []) {
    this._repository = repository;
    this._populateOnFind = populateOnFind;
  }

  getAll(): Promise<T[]> {
    return this._repository.find().populate(this._populateOnFind).exec();
  }

  count(): Promise<number> {
    return this._repository.count().populate(this._populateOnFind).exec();
  }

  getById(id: any): Promise<any> {
    return this._repository.findById(id).populate(this._populateOnFind).exec();
  }

  get(filter: any): Promise<any> {
    let parsedFilter = filter;

    if (typeof filter === 'string') {
      parsedFilter = JSON.parse(filter);
    }

    return this._repository.find(parsedFilter).populate(this._populateOnFind).exec();
  }

  getOne(filter: any): Promise<any> {
    return this._repository
      .findOne(filter)
      .populate(this._populateOnFind)
      .exec();
  }

  create(item: T): Promise<T> {
    return this._repository.create(item);
  }

  createMany(items: T[]): Promise<T[]> {
    return this._repository.insertMany(items);
  }

  update(id: string, item: T) {
    return this._repository.findByIdAndUpdate(id, item);
  }

  aggregate(aggregateFilter: any): any {
    return this._repository.aggregate(aggregateFilter);
  }
}
