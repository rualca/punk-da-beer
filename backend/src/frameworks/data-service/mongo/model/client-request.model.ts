import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type ClientRequestDocument = ClientRequest & Document;

@Schema()
export class ClientRequest {
  // @Prop({
  //   required: true,
  //   unique: true,
  // })
  // id: string;

  @Prop({
    required: true,
  })
  method: string;

  @Prop({
    required: true,
  })
  url: string;

  @Prop({
    required: true,
  })
  headers: string[];

  @Prop({
    type: Object,
  })
  body: any;

  @Prop({
    type: Object,
  })
  query: any;

  @Prop({
    type: Object,
  })
  params: any;
}

export const ClientRequestSchema = SchemaFactory.createForClass(ClientRequest);
