import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type BeerDocument = Beer & Document;

@Schema()
export class Beer {
  @Prop({
    required: true,
    unique: true,
  })
  id: string;

  @Prop({
    required: true,
    unique: true,
  })
  name: string;

  @Prop()
  tagline: string;

  @Prop()
  first_brewed: string;

  @Prop()
  description: string;

  @Prop()
  image_url: string;

  @Prop()
  abv: number;

  @Prop()
  target_fg: number;

  @Prop()
  target_og: number;

  @Prop()
  ebc: number;

  @Prop()
  srm: number;

  @Prop()
  ph: number;

  @Prop()
  attenuation_level: number;

  @Prop({
    type: Object,
  })
  volume: {
    value: number;
    unit: string;
  };

  @Prop({
    type: Object,
  })
  boil_volume: {
    value: number;
    unit: string;
  };

  @Prop({
    type: Object,
  })
  method: {
    mash_temp: {
      temp: {
        value: number;
        unit: string;
      };
      duration: string;
    };
    fermentation: {
      temp: {
        value: number;
        unit: string;
      };
    };
    twist: string;
  };

  @Prop({
    type: Object,
  })
  ingredients: {
    malt: {
      name: string;
      amount: {
        value: number;
        unit: string;
      };
    };
    hops: {
      name: string;
      amount: {
        value: number;
        unit: string;
      };
      add: string;
      attribute: string;
    }[];
    yeast: string;
  };

  @Prop()
  food_pairing: string[];

  @Prop()
  brewers_tips: string;

  @Prop()
  contributed_by: string;
}

export const BeerSchema = SchemaFactory.createForClass(Beer);
