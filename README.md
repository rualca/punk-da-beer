<p>
  <a href="https://gitlab.com/rualca/punk-da-beer" target="_blank">
    <img alt="beer-logo" height="200" alt="Beer Logo" src="https://media.istockphoto.com/vectors/beer-and-barley-vector-label-vector-id498536314?b=1&k=20&m=498536314&s=170667a&w=0&h=V9Xv4W-_h9pHNN3-ArR3k3j-u2UyqsqHGpQ2Q9Er0fA="/>
  </a>

  <a href="code_of_conduct.md">
    <img src="https://img.shields.io/badge/Contributor%20Covenant-v2.0%20adopted-ff69b4.svg" alt="Contributor Covenant">
  </a>
  
  [![Website shields.io](https://img.shields.io/website-up-down-green-red/http/shields.io.svg)](http://shields.io/)
  [![Commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg)](http://commitizen.github.io/cz-cli/)
  [![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)
  [![made-with-Markdown](https://img.shields.io/badge/Made%20with-Markdown-1f425f.svg)](http://commonmark.org)
  [![made-with-javascript](https://img.shields.io/badge/Made%20with-JavaScript-1f425f.svg)](https://www.javascript.com)
  [![Minimum node.js version](https://badgen.net/npm/node/express)](https://npmjs.com/package/node)

  [![GitLab last commit](https://badgen.net/gitlab/last-commit/rualca/punk-da-beer/)](https://gitlab.com/rualca/punk-da-beer/-/commits)
  [![GitLab branch](https://badgen.net/gitlab/branches/rualca/punk-da-beer/)](https://gitlab.com/rualca/punk-da-beer/-/branches)
  [![Docker](https://badgen.net/badge/icon/docker?icon=docker&label)](https://https://docker.com/)
</p>
  
PunkDaBeer is an open‑source beer listing.

# Backend Features

The backend have:

- A public route to login
- A private route to get a beer
- A private route to autocomplete beer name or ingredient
- A private route to get a beer based on main filters(name, ingredients)
- A private route to get the top 10 ingredients more repeated
- Saved each request in DB, to audit client information

And of course, this project counts with:

- Clarity and structure
- Use of SOLID principles
- Use of patterns
- Tests

# Frontend Features

The frontend have:

  - A login page
  - A list of beers private page that lists all beers. And a autocomplete to filter beers by name or ingredient, in the right part you must show a top 10 list of beers.

  <aside>
  💡 You must use a single input for this autocomplete. The options displayed will be the ingredients or names that match part of the text entered. When the user chooses a valid ingredient or name from those available, the Top 10 beer list should be updated ordered by `first_brewed` field

  </aside>

  - A detailed private page of a beer(image, description, etc...), lets your creativity fly.

  And of course, this project counts with:

- Clarity and structure
- Custom components and style
- Methodology and reusable principles
- Tests

# Getting Started

You can get started with PunkDaBeer immediately executing:

```
docker-compose up
```